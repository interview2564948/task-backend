<?php

declare(strict_types=1);

namespace App\Modules\Invoices\Application\DTO;

/**
 * @internal
 */
final readonly class BilledCompanyDTO
{
    public function __construct(
        public string $name,
        public string $street,
        public string $city,
        public string $zip,
        public string $phone,
        public string $email
    ) {
    }
}
