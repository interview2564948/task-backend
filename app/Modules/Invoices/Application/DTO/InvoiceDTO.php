<?php

declare(strict_types=1);

namespace App\Modules\Invoices\Application\DTO;

final readonly class InvoiceDTO
{
    public function __construct(
        public string $invoiceNumber,
        public string $invoiceDate,
        public string $dueDate,
        public CompanyDTO $company,
        public BilledCompanyDTO $billedCompany,
        public array $products,
        public int $totalPrice
    ) {
    }
}
