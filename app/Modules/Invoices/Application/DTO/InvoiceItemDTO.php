<?php

declare(strict_types=1);

namespace App\Modules\Invoices\Application\DTO;

/**
 * @internal
 */
final readonly class InvoiceItemDTO
{
    public function __construct(public string $name, public int $quantity, public int $unitPrice, public int $total)
    {
    }
}
