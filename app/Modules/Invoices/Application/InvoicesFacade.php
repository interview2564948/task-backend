<?php

declare(strict_types=1);

namespace App\Modules\Invoices\Application;

use App\Domain\Enums\StatusEnum;
use App\Modules\Approval\Api\ApprovalFacadeInterface;
use App\Modules\Approval\Api\Dto\ApprovalDto;
use App\Modules\Invoices\Application\DTO\InvoiceDTO;
use App\Modules\Invoices\Application\Services\InvoiceApplicationService;
use App\Modules\Invoices\Domain\Entities\Invoice;
use App\Modules\Invoices\Domain\Repositories\InvoicesRepositoryInterface;
use Ramsey\Uuid\UuidInterface;

final readonly class InvoicesFacade implements InvoicesFacadeInterface
{
    public function __construct(
        private ApprovalFacadeInterface $approvalFacade,
        private InvoiceApplicationService $applicationService,
        private InvoicesRepositoryInterface $invoicesRepository,
    ) {
    }

    public function approve(UuidInterface $uuid): void
    {
        $invoice = $this->invoicesRepository->getById($uuid->toString());

        $this->approvalFacade->approve(
            new ApprovalDto(
                $invoice->getId(),
                StatusEnum::from($invoice->getStatus()->getStatus()),
                Invoice::class
            )
        );
    }

    public function reject(UuidInterface $uuid): void
    {
        $invoice = $this->invoicesRepository->getById($uuid->toString());

        $this->approvalFacade->reject(
            new ApprovalDto(
                $invoice->getId(),
                StatusEnum::from($invoice->getStatus()->getStatus()),
                Invoice::class
            )
        );
    }

    public function getInvoice(UuidInterface $uuid): InvoiceDTO
    {
        return $this->applicationService->getInvoiceDto($uuid->toString());
    }
}
