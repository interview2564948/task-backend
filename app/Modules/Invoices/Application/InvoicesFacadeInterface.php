<?php

declare(strict_types=1);

namespace App\Modules\Invoices\Application;

use App\Modules\Invoices\Application\DTO\InvoiceDTO;
use Ramsey\Uuid\UuidInterface;

interface InvoicesFacadeInterface
{
    public function approve(UuidInterface $uuid): void;

    public function reject(UuidInterface $uuid): void;

    public function getInvoice(UuidInterface $uuid): InvoiceDTO;
}
