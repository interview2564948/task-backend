<?php

declare(strict_types=1);

namespace App\Modules\Invoices\Application\Listeners;

use App\Modules\Approval\Api\Events\EntityApproved;
use App\Modules\Invoices\Domain\Entities\Invoice;
use App\Modules\Invoices\Domain\Repositories\InvoicesRepositoryInterface;

/**
 * @internal
 */
class InvoiceApprovalListener
{
    public function __construct(private InvoicesRepositoryInterface $invoicesRepository)
    {
    }


    public function handle(EntityApproved $event): void
    {
        if (Invoice::class !== $event->approvalDto->entity) {
            return;
        }
        $invoice = $this->invoicesRepository->getById($event->approvalDto->id->toString());
        $invoice->approve();
        $this->invoicesRepository->save($invoice);
    }
}
