<?php

declare(strict_types=1);

namespace App\Modules\Invoices\Application\Listeners;

use App\Modules\Approval\Api\Events\EntityRejected;
use App\Modules\Invoices\Domain\Entities\Invoice;
use App\Modules\Invoices\Domain\Repositories\InvoicesRepositoryInterface;

/**
 * @internal
 */
class InvoiceRejectionListener
{
    public function __construct(private InvoicesRepositoryInterface $invoicesRepository)
    {
    }


    public function handle(EntityRejected $event): void
    {
        if (Invoice::class !== $event->approvalDto->entity) {
            return;
        }
        $invoice = $this->invoicesRepository->getById($event->approvalDto->id->toString());
        $invoice->reject();
        $this->invoicesRepository->save($invoice);
    }
}
