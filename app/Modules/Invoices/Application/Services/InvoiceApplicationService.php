<?php

declare(strict_types=1);

namespace App\Modules\Invoices\Application\Services;

use App\Modules\Invoices\Application\DTO\BilledCompanyDTO;
use App\Modules\Invoices\Application\DTO\CompanyDTO;
use App\Modules\Invoices\Application\DTO\InvoiceDTO;
use App\Modules\Invoices\Application\DTO\InvoiceItemDTO;
use App\Modules\Invoices\Domain\Entities\InvoiceItem;
use App\Modules\Invoices\Domain\Repositories\InvoicesRepositoryInterface;

/**
 * @internal
 */
class InvoiceApplicationService
{
    private InvoicesRepositoryInterface $invoicesRepository;

    public function __construct(InvoicesRepositoryInterface $invoicesRepository)
    {
        $this->invoicesRepository = $invoicesRepository;
    }

    public function getInvoiceDto(string $id): InvoiceDTO
    {
        $invoice = $this->invoicesRepository->getById($id);

        return new InvoiceDTO(
            $invoice->getNumber(),
            $invoice->getDate()->getFormattedDate(),
            $invoice->getDueDate()->getFormattedDate(),
            new CompanyDTO(
                $invoice->getFromCompany()->getName(),
                $invoice->getFromCompany()->getAddress()->getStreet(),
                $invoice->getFromCompany()->getAddress()->getCity(),
                $invoice->getFromCompany()->getAddress()->getZip(),
                $invoice->getFromCompany()->getPhoneNumber()->getPhoneNumber()
            ),
            new BilledCompanyDTO(
                $invoice->getBilledCompany()->getName(),
                $invoice->getBilledCompany()->getAddress()->getStreet(),
                $invoice->getBilledCompany()->getAddress()->getCity(),
                $invoice->getBilledCompany()->getAddress()->getZip(),
                $invoice->getBilledCompany()->getPhoneNumber()->getPhoneNumber(),
                $invoice->getBilledCompany()->getEmail()
            ),
            array_map(function (InvoiceItem $invoiceItem) {
                return new InvoiceItemDTO(
                    $invoiceItem->getName(),
                    $invoiceItem->getQuantity(),
                    $invoiceItem->getPrice()->getValue(),
                    $invoiceItem->calculatePrice()
                );
            }, $invoice->getInvoiceItems()),
            $invoice->calculateTotalPrice()
        );
    }
}
