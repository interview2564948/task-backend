<?php

declare(strict_types=1);

namespace App\Modules\Invoices\Domain\Entities;

use App\Modules\Invoices\Domain\ValueObjects\Address;
use App\Modules\Invoices\Domain\ValueObjects\PhoneNumber;
use Ramsey\Uuid\UuidInterface;

class Company
{
    private UuidInterface $id;
    private string $name;
    private Address $address;
    private PhoneNumber $phoneNumber;
    private string $email;
    public function __construct(UuidInterface $id, string $name, Address $address, PhoneNumber $phoneNumber, $email)
    {
        $this->id = $id;
        $this->name = $name;
        $this->address = $address;
        $this->phoneNumber = $phoneNumber;
        $this->email = $email;
    }

    public function getId(): UuidInterface
    {
        return $this->id;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function getAddress(): Address
    {
        return $this->address;
    }

    public function getPhoneNumber(): PhoneNumber
    {
        return $this->phoneNumber;
    }

    public function getEmail(): string
    {
        return $this->email;
    }


}
