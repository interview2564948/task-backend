<?php

declare(strict_types=1);

namespace App\Modules\Invoices\Domain\Entities;

use App\Domain\Enums\StatusEnum;
use App\Modules\Invoices\Domain\ValueObjects\Date;
use App\Modules\Invoices\Domain\ValueObjects\DueDate;
use App\Modules\Invoices\Domain\ValueObjects\Status;
use Ramsey\Uuid\UuidInterface;

class Invoice
{
    private UuidInterface $id;
    private string $number;
    private Company $fromCompany;
    private Company $billedCompany;
    private Date $date;
    private DueDate $dueDate;
    private Status $status;
    private array $invoiceItems = [];
    public function __construct(
        UuidInterface $id,
        string $number,
        Company $fromCompany,
        Company $billedCompany,
        Date $date,
        DueDate $dueDate,
        Status $status,
        array $invoiceItems
    ) {
        $this->id = $id;
        $this->number = $number;
        $this->fromCompany = $fromCompany;
        $this->billedCompany = $billedCompany;
        $this->date = $date;
        $this->dueDate = $dueDate;
        $this->status = $status;
        $this->invoiceItems = $invoiceItems;
    }

    public function calculateTotalPrice(): int
    {
        $total = 0;
        foreach ($this->invoiceItems as $invoiceItem) {
            $total += $invoiceItem->calculatePrice();
        }

        return $total;
    }

    /// in bigger system this the place when other action could be done when approve or reject is performed
    public function approve(): void
    {
        if (!$this->status->isDraft()) {
            throw new \LogicException('approval status is already assigned');
        }

        $this->status = new Status(StatusEnum::APPROVED->value);
    }

    public function reject(): void
    {
        if (!$this->status->isDraft()) {
            throw new \LogicException('approval status is already assigned');
        }

        $this->status = new Status(StatusEnum::REJECTED->value);
    }

    public function getId(): UuidInterface
    {
        return $this->id;
    }

    public function getNumber(): string
    {
        return $this->number;
    }

    public function getFromCompany(): Company
    {
        return $this->fromCompany;
    }

    public function getBilledCompany(): Company
    {
        return $this->billedCompany;
    }

    public function getDate(): Date
    {
        return $this->date;
    }

    public function getDueDate(): DueDate
    {
        return $this->dueDate;
    }

    public function getStatus(): Status
    {
        return $this->status;
    }

    /**
     * @return array<InvoiceItem>
     */
    public function getInvoiceItems(): array
    {
        return $this->invoiceItems;
    }

    private function canChangeStatus(): bool
    {
        return $this->status->isDraft();
    }
}
