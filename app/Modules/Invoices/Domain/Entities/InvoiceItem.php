<?php

declare(strict_types=1);

namespace App\Modules\Invoices\Domain\Entities;

use App\Modules\Invoices\Domain\ValueObjects\Money;
use Ramsey\Uuid\UuidInterface;

/**
 * @internal
 */
class InvoiceItem
{
    private UuidInterface $id;
    private string $name;
    private int $quantity;
    private Money $price;
    public function __construct(UuidInterface $id, string $name, int $quantity, Money $price)
    {
        $this->id = $id;
        $this->name = $name;
        $this->quantity = $quantity;
        $this->price = $price;
    }

    public function calculatePrice(): int
    {
        return intval($this->quantity * $this->price->getValue());
    }

    public function getId(): UuidInterface
    {
        return $this->id;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function getQuantity(): int
    {
        return $this->quantity;
    }

    public function getPrice(): Money
    {
        return $this->price;
    }
}
