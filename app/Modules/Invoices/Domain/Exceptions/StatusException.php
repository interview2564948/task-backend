<?php

declare(strict_types=1);

namespace App\Modules\Invoices\Domain\Exceptions;

use Exception;

class StatusException extends Exception
{
}
