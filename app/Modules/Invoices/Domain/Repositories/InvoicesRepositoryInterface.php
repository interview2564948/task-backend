<?php

declare(strict_types=1);

namespace App\Modules\Invoices\Domain\Repositories;

use App\Modules\Invoices\Domain\Entities\Invoice;

interface InvoicesRepositoryInterface
{
    public function getById(string $id): Invoice;
    public function save(Invoice $invoice): void;
}
