<?php

declare(strict_types=1);

namespace App\Modules\Invoices\Domain\ValueObjects;

final readonly class Address
{
    private string $street;
    private string $city;
    private string $zip;

    public function __construct(string $street, string $city, string $zip)
    {
        // validation if needed
        $this->street = $street;
        $this->city = $city;
        $this->zip = $zip;
    }

    public function getStreet(): string
    {
        return $this->street;
    }

    public function getCity(): string
    {
        return $this->city;
    }

    public function getZip(): string
    {
        return $this->zip;
    }
}
