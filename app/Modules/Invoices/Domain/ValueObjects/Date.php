<?php

declare(strict_types=1);

namespace App\Modules\Invoices\Domain\ValueObjects;

use DateTime;

final readonly class Date
{
    private DateTime $date;

    public function __construct(DateTime $date)
    {
        /// validation if needed
        $this->date = $date;
    }

    public function getDate(): DateTime
    {
        return $this->date;
    }

    public function getFormattedDate(): string
    {
        return $this->date->format('Y-m-d');
    }
}
