<?php

declare(strict_types=1);

namespace App\Modules\Invoices\Domain\ValueObjects;

use DateTime;

final readonly class DueDate
{
    private DateTime $dueDate;
    public function __construct(DateTime $dueDate)
    {
        /// maybe it's good place to check id due date is in the future
        $this->dueDate = $dueDate;
    }
    public function getDate(): DateTime
    {
        return $this->dueDate;
    }
    public function getFormattedDate(): string
    {
        return $this->dueDate->format('Y-m-d');
    }
}
