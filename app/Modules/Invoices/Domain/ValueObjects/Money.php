<?php

declare(strict_types=1);

namespace App\Modules\Invoices\Domain\ValueObjects;

final readonly class Money
{
    private int $value;
    private string $currency;

    public function __construct(int $value, string $currency)
    {
        if ('usd' !== $currency) {
            throw new \LogicException('Currency cannot be other than usd');
        }
        $this->value = $value;
        $this->currency = $currency;
    }

    public function getValue(): int
    {
        return $this->value;
    }

    public function getCurrency(): string
    {
        return $this->currency;
    }
}
