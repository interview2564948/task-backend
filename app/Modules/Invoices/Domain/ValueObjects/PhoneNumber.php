<?php

declare(strict_types=1);

namespace App\Modules\Invoices\Domain\ValueObjects;

final readonly class PhoneNumber
{
    private string $phoneNumber;

    public function __construct(string $phoneNumber)
    {
        /// validation if needed
        $this->phoneNumber = $phoneNumber;
    }
    public function getPhoneNumber(): string
    {
        return $this->phoneNumber;
    }
}
