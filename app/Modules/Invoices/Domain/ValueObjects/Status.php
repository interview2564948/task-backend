<?php

declare(strict_types=1);

namespace App\Modules\Invoices\Domain\ValueObjects;

use App\Domain\Enums\StatusEnum;
use App\Modules\Invoices\Domain\Exceptions\StatusException;

final readonly class Status
{
    private string $status;

    public function __construct(string $status)
    {
        if (null === StatusEnum::tryFrom($status)) {
            throw new StatusException('Wrong invoice status provided.');
        }
        $this->status = $status;
    }

    public function isDraft(): bool
    {
        return $this->status === StatusEnum::DRAFT->value;
    }

    public function getStatus(): string
    {
        return $this->status;
    }
}
