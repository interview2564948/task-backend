<?php

declare(strict_types=1);

namespace App\Modules\Invoices\Infrastructure\Database\Repositories;

use App\Modules\Invoices\Domain\Entities\Invoice;
use App\Modules\Invoices\Domain\Repositories\InvoicesRepositoryInterface;

/**
 * @internal
 */
class InMemoryInvoicesRepository implements InvoicesRepositoryInterface
{
    private array $invoices = [];

    public function getById(string $id): Invoice
    {
        $invoice = $this->invoices[$id] ?? null;
        if (null === $invoice) {
            throw new \Exception('Invoice not found');
        }

        return $invoice;
    }

    public function save(Invoice $invoice): void
    {
        $this->invoices[$invoice->getId()->toString()] = $invoice;
    }

    public function add(Invoice $invoice): void
    {
        if (isset($this->invoices[$invoice->getId()->toString()])) {
            throw new \Exception("Invoice with ID {$invoice->getId()->toString()} already exists");
        }

        $this->invoices[$invoice->getId()->toString()] = $invoice;
    }
}
