<?php

declare(strict_types=1);

namespace App\Modules\Invoices\Infrastructure\Database\Repositories;

use App\Modules\Invoices\Domain\Entities\Invoice;
use App\Modules\Invoices\Domain\Repositories\InvoicesRepositoryInterface;
use App\Modules\Invoices\Infrastructure\Mappers\InvoiceMapper;
use Exception;
use Illuminate\Support\Facades\DB;

/**
 * @internal
 */
class InvoicesRepository implements InvoicesRepositoryInterface
{
    private InvoiceMapper $invoiceMapper;

    public function __construct(InvoiceMapper $invoiceMapper)
    {
        $this->invoiceMapper = $invoiceMapper;
    }

    public function getById(string $id): Invoice
    {

        $invoiceData = DB::table('invoices')
            ->select([
                'invoices.id as invoice_id',
                'invoices.number as invoice_number',
                'invoices.date as invoice_date',
                'invoices.due_date as invoice_due_date',
                'invoices.status as invoice_status',
                'products.id as product_id',
                'products.name as product_name',
                'products.price as product_price',
                'products.currency as product_currency',
                'invoice_product_lines.quantity as product_quantity',
                'companies.id as company_id',
                'companies.name as company_name',
                'companies.street as company_street',
                'companies.city as company_city',
                'companies.zip as company_zip',
                'companies.phone as company_phone_number',
                'companies.email as company_email',
            ])
            ->leftJoin(
                'invoice_product_lines',
                'invoices.id',
                '=',
                'invoice_product_lines.invoice_id'
            )
            ->leftJoin('products', 'products.id', '=', 'invoice_product_lines.product_id')
            ->leftJoin('companies', 'companies.id', '=', 'invoices.company_id')
            ->where('invoice_id', $id)->get();


        if ($invoiceData->isEmpty()) {
            throw new Exception('Cannot find invoice');
        }

        return $this->invoiceMapper->toEntity($invoiceData->all());
    }

    public function save(Invoice $invoice): void
    {
        DB::table('invoices')->where('id', $invoice->getId()->toString())
            ->update([
                'number' => $invoice->getNumber(),
                'date' => $invoice->getDate()->getDate(),
                'due_date' => $invoice->getDueDate()->getDate(),
                'company_id' => $invoice->getFromCompany()->getId()->toString(),
                'status' => $invoice->getStatus()->getStatus(),
            ]);
    }
}
