<?php

declare(strict_types=1);

namespace App\Modules\Invoices\Infrastructure\Http;

use App\Infrastructure\Controller;
use App\Modules\Invoices\Application\InvoicesFacade;
use Exception;
use Ramsey\Uuid\Uuid;

class InvoicesController extends Controller
{
    public function __construct(private InvoicesFacade $invoicesFacade)
    {
    }

    public function show(string $uuid): false|string
    {
        try {
            $invoice = $this->invoicesFacade->getInvoice(Uuid::fromString($uuid));
            return json_encode($invoice);
        } catch (Exception $exception) {
            return json_encode(['error' => 'Cannot show invoice']);
        }
    }
    public function approve(string $uuid): string
    {
        try {
            $this->invoicesFacade->approve(Uuid::fromString($uuid));
            return json_encode(['success' => 'Invoice approved']);
        } catch (Exception $exception) {
            return json_encode(['error' => $exception->getMessage()]);
        }
    }

    public function reject(string $uuid): string
    {
        try {
            $this->invoicesFacade->reject(Uuid::fromString($uuid));
            return json_encode(['success' => 'Invoice rejected']);
        } catch (Exception $exception) {
            return json_encode(['error' => $exception->getMessage()]);
        }
    }
}
