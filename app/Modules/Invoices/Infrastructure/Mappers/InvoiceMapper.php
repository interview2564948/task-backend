<?php

declare(strict_types=1);

namespace App\Modules\Invoices\Infrastructure\Mappers;

use App\Modules\Invoices\Domain\Entities\Company;
use App\Modules\Invoices\Domain\Entities\Invoice;
use App\Modules\Invoices\Domain\Entities\InvoiceItem;
use App\Modules\Invoices\Domain\ValueObjects\Address;
use App\Modules\Invoices\Domain\ValueObjects\Date;
use App\Modules\Invoices\Domain\ValueObjects\DueDate;
use App\Modules\Invoices\Domain\ValueObjects\Money;
use App\Modules\Invoices\Domain\ValueObjects\PhoneNumber;
use App\Modules\Invoices\Domain\ValueObjects\Status;
use DateTime;
use Ramsey\Uuid\Uuid;

class InvoiceMapper
{
    public function toEntity(array $invoiceData): Invoice
    {
        $invoice = $invoiceData[0];

        return new Invoice(
            Uuid::fromString($invoice->invoice_id),
            $invoice->invoice_number,
            new Company(
                Uuid::fromString($invoice->company_id),
                $invoice->company_name,
                new Address($invoice->company_street, $invoice->company_city, $invoice->company_zip),
                new PhoneNumber($invoice->company_phone_number),
                $invoice->company_email
            ),
            new Company(
                Uuid::fromString($invoice->company_id),
                $invoice->company_name,
                new Address($invoice->company_street, $invoice->company_city, $invoice->company_zip),
                new PhoneNumber($invoice->company_phone_number),
                $invoice->company_email
            ),
            new Date(new DateTime($invoice->invoice_date)),
            new DueDate(new DateTime($invoice->invoice_due_date)),
            new Status($invoice->invoice_status),
            array_map(function ($invoiceItem) {
                return new InvoiceItem(
                    Uuid::fromString($invoiceItem->product_id),
                    $invoiceItem->product_name,
                    $invoiceItem->product_quantity,
                    new Money($invoiceItem->product_price, $invoiceItem->product_currency)
                );
            }, $invoiceData)
        );
    }
}
