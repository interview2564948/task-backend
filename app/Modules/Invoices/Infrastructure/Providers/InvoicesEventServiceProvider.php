<?php

declare(strict_types=1);

namespace App\Modules\Invoices\Infrastructure\Providers;

use App\Modules\Approval\Api\Events\EntityApproved;
use App\Modules\Approval\Api\Events\EntityRejected;
use App\Modules\Invoices\Application\Listeners\InvoiceApprovalListener;
use App\Modules\Invoices\Application\Listeners\InvoiceRejectionListener;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\Event;

class InvoicesEventServiceProvider extends ServiceProvider
{
    public function boot(): void
    {
        Event::listen(EntityApproved::class, [InvoiceApprovalListener::class, 'handle']);
        Event::listen(EntityRejected::class, [InvoiceRejectionListener::class, 'handle']);
    }
}
