<?php

declare(strict_types=1);

namespace App\Modules\Invoices\Infrastructure\Providers;

use App\Infrastructure\Providers\RouteServiceProvider;
use Illuminate\Support\Facades\Route;

class InvoicesRoutesServiceProvider extends RouteServiceProvider
{
    public function boot(): void
    {
        $this->routes(function (): void {
            Route::prefix('invoice')
                ->group(base_path('app/Modules/Invoices/Infrastructure/routes/routes.php'));
        });
    }
}
