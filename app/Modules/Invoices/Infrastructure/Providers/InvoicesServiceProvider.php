<?php

declare(strict_types=1);

namespace App\Modules\Invoices\Infrastructure\Providers;

use App\Modules\Invoices\Application\InvoicesFacade;
use App\Modules\Invoices\Application\InvoicesFacadeInterface;
use App\Modules\Invoices\Application\Services\InvoiceApplicationService;
use App\Modules\Invoices\Domain\Repositories\InvoicesRepositoryInterface;
use App\Modules\Invoices\Infrastructure\Database\Repositories\InvoicesRepository;
use Illuminate\Contracts\Support\DeferrableProvider;
use Illuminate\Support\ServiceProvider;

class InvoicesServiceProvider extends ServiceProvider implements DeferrableProvider
{
    public function register(): void
    {
        $this->app->scoped(InvoicesFacadeInterface::class, InvoicesFacade::class);
        $this->app->scoped(InvoicesRepositoryInterface::class, InvoicesRepository::class);
        $this->app->scoped(InvoiceApplicationService::class, InvoiceApplicationService::class);
    }

    /** @return array<class-string> */
    public function provides(): array
    {
        return [
            InvoicesFacadeInterface::class,
            InvoicesRepositoryInterface::class,
            InvoiceApplicationService::class,
        ];
    }
}
