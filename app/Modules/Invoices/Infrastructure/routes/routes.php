<?php

declare(strict_types=1);

use App\Modules\Invoices\Infrastructure\Http\InvoicesController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/// I know that I'm using get to processing approvals, but it's just simplified for interview-task purposes

Route::get('/{id}', [InvoicesController::class, 'show']);
Route::get('/{id}/approve', [InvoicesController::class, 'approve']);
Route::get('/{id}/reject', [InvoicesController::class, 'reject']);
