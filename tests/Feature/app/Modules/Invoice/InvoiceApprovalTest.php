<?php

declare(strict_types=1);

namespace Tests\Feature\app\Modules\Invoice;

use App\Domain\Enums\StatusEnum;
use App\Modules\Invoices\Domain\Entities\Company;
use App\Modules\Invoices\Domain\Entities\Invoice;
use App\Modules\Invoices\Domain\Entities\InvoiceItem;
use App\Modules\Invoices\Domain\Repositories\InvoicesRepositoryInterface;
use App\Modules\Invoices\Domain\ValueObjects\Address;
use App\Modules\Invoices\Domain\ValueObjects\Date;
use App\Modules\Invoices\Domain\ValueObjects\DueDate;
use App\Modules\Invoices\Domain\ValueObjects\Money;
use App\Modules\Invoices\Domain\ValueObjects\PhoneNumber;
use App\Modules\Invoices\Domain\ValueObjects\Status;
use App\Modules\Invoices\Infrastructure\Database\Repositories\InMemoryInvoicesRepository;
use Ramsey\Uuid\Uuid;
use Ramsey\Uuid\UuidInterface;
use Tests\TestCase;

class InvoiceApprovalTest extends TestCase
{
    private InvoicesRepositoryInterface $invoicesRepository;
    private UuidInterface $invoiceId;

    public function test_id_approves_invoice(): void
    {
        $this->app->singleton(InvoicesRepositoryInterface::class, InMemoryInvoicesRepository::class);
        $this->invoicesRepository = $this->app->make(InvoicesRepositoryInterface::class);

        $this->givenInvoice();
        $this->whenApproves();
        $this->thenInvoiceIsApproved();
    }

    private function givenInvoice(): void
    {
        $this->invoiceId = Uuid::uuid4();

        $invoiceItems = [
            new InvoiceItem(Uuid::uuid4(), 'Product1', 2, new Money(10, 'usd')),
            new InvoiceItem(Uuid::uuid4(), 'Product1', 4, new Money(20, 'usd')),
        ];

        $invoice =  new Invoice(
            $this->invoiceId,
            '555',
            new Company(
                Uuid::uuid4(),
                'Company',
                new Address('Wall Street', 'NY', '00110'),
                new PhoneNumber('22222222'),
                'email@example.com'
            ),
            new Company(
                Uuid::uuid4(),
                'BilledCompany',
                new Address('Wall Street', 'NY', '00110'),
                new PhoneNumber('11111111'),
                'email@example.com'
            ),
            new Date(new \DateTime()),
            new DueDate(new \DateTime()),
            new Status(StatusEnum::DRAFT->value),
            $invoiceItems
        );

        $this->invoicesRepository->add($invoice);
    }

    private function whenApproves(): void
    {
        $this->get('invoice/' . $this->invoiceId->toString() . '/approve');
    }

    private function thenInvoiceIsApproved(): void
    {
        $invoice = $this->invoicesRepository->getById($this->invoiceId->toString());

        self::assertEquals($invoice->getStatus()->getStatus(), StatusEnum::APPROVED->value);
    }
}
