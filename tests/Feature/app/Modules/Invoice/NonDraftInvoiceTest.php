<?php

declare(strict_types=1);

namespace Tests\Feature\app\Modules\Invoice;

use App\Domain\Enums\StatusEnum;
use App\Modules\Invoices\Domain\Entities\Company;
use App\Modules\Invoices\Domain\Entities\Invoice;
use App\Modules\Invoices\Domain\Entities\InvoiceItem;
use App\Modules\Invoices\Domain\Repositories\InvoicesRepositoryInterface;
use App\Modules\Invoices\Domain\ValueObjects\Address;
use App\Modules\Invoices\Domain\ValueObjects\Date;
use App\Modules\Invoices\Domain\ValueObjects\DueDate;
use App\Modules\Invoices\Domain\ValueObjects\Money;
use App\Modules\Invoices\Domain\ValueObjects\PhoneNumber;
use App\Modules\Invoices\Domain\ValueObjects\Status;
use App\Modules\Invoices\Infrastructure\Database\Repositories\InMemoryInvoicesRepository;
use Illuminate\Testing\TestResponse;
use Ramsey\Uuid\Uuid;
use Ramsey\Uuid\UuidInterface;
use Tests\TestCase;

class NonDraftInvoiceTest extends TestCase
{
    private InvoicesRepositoryInterface $invoicesRepository;
    private UuidInterface $invoiceId;
    private TestResponse $response;

    public function test_it_doesnt_change_approved_invoice_status(): void
    {
        $this->app->singleton(InvoicesRepositoryInterface::class, InMemoryInvoicesRepository::class);
        $this->invoicesRepository = $this->app->make(InvoicesRepositoryInterface::class);

        $this->givenApprovedInvoice();
        $this->whenRejects();
        $this->thenApprovedInvoiceIsUnchanged();
    }

    public function test_it_doesnt_change_rejected_invoice_status(): void
    {
        $this->app->singleton(InvoicesRepositoryInterface::class, InMemoryInvoicesRepository::class);
        $this->invoicesRepository = $this->app->make(InvoicesRepositoryInterface::class);

        $this->givenRejectedInvoice();
        $this->whenApproves();
        $this->thenRejectedInvoiceIsUnchanged();
    }

    private function givenApprovedInvoice(): void
    {
        $this->invoiceId = Uuid::uuid4();

        $invoiceItems = [
            new InvoiceItem(Uuid::uuid4(), 'Product1', 2, new Money(10, 'usd')),
            new InvoiceItem(Uuid::uuid4(), 'Product1', 4, new Money(20, 'usd')),
        ];

        $invoice = new Invoice(
            $this->invoiceId,
            '555',
            new Company(
                Uuid::uuid4(),
                'Company',
                new Address('Wall Street', 'NY', '00110'),
                new PhoneNumber('22222222'),
                'email@example.com'
            ),
            new Company(
                Uuid::uuid4(),
                'BilledCompany',
                new Address('Wall Street', 'NY', '00110'),
                new PhoneNumber('11111111'),
                'email@example.com'
            ),
            new Date(new \DateTime()),
            new DueDate(new \DateTime()),
            new Status(StatusEnum::APPROVED->value),
            $invoiceItems
        );

        $this->invoicesRepository->add($invoice);
    }

    private function givenRejectedInvoice(): void
    {
        $this->invoiceId = Uuid::uuid4();

        $invoiceItems = [
            new InvoiceItem(Uuid::uuid4(), 'Product1', 2, new Money(10, 'usd')),
            new InvoiceItem(Uuid::uuid4(), 'Product1', 4, new Money(20, 'usd')),
        ];

        $invoice = new Invoice(
            $this->invoiceId,
            '555',
            new Company(
                Uuid::uuid4(),
                'Company',
                new Address('Wall Street', 'NY', '00110'),
                new PhoneNumber('22222222'),
                'email@example.com'
            ),
            new Company(
                Uuid::uuid4(),
                'BilledCompany',
                new Address('Wall Street', 'NY', '00110'),
                new PhoneNumber('11111111'),
                'email@example.com'
            ),
            new Date(new \DateTime()),
            new DueDate(new \DateTime()),
            new Status(StatusEnum::REJECTED->value),
            $invoiceItems
        );

        $this->invoicesRepository->add($invoice);
    }

    private function whenRejects(): void
    {
        $this->response = $this->get('invoice/' . $this->invoiceId->toString() . '/reject');
    }

    private function whenApproves(): void
    {
        $this->response = $this->get('invoice/' . $this->invoiceId->toString() . '/approve');
    }

    private function thenApprovedInvoiceIsUnchanged(): void
    {
        $this->response->assertExactJson(['error' => 'approval status is already assigned']);

        $invoice = $this->invoicesRepository->getById($this->invoiceId->toString());
        self::assertEquals($invoice->getStatus()->getStatus(), StatusEnum::APPROVED->value);
    }

    private function thenRejectedInvoiceIsUnchanged(): void
    {
        $this->response->assertExactJson(['error' => 'approval status is already assigned']);

        $invoice = $this->invoicesRepository->getById($this->invoiceId->toString());
        self::assertEquals($invoice->getStatus()->getStatus(), StatusEnum::REJECTED->value);
    }
}
