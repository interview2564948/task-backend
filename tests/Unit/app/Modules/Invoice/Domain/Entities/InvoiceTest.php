<?php

declare(strict_types=1);

namespace Tests\Unit\app\Modules\Invoice\Domain\Entities;

use App\Domain\Enums\StatusEnum;
use App\Modules\Invoices\Domain\Entities\Company;
use App\Modules\Invoices\Domain\Entities\Invoice;
use App\Modules\Invoices\Domain\Entities\InvoiceItem;
use App\Modules\Invoices\Domain\ValueObjects\Address;
use App\Modules\Invoices\Domain\ValueObjects\Date;
use App\Modules\Invoices\Domain\ValueObjects\DueDate;
use App\Modules\Invoices\Domain\ValueObjects\Money;
use App\Modules\Invoices\Domain\ValueObjects\PhoneNumber;
use App\Modules\Invoices\Domain\ValueObjects\Status;
use PHPUnit\Framework\TestCase;
use Ramsey\Uuid\Uuid;

class InvoiceTest extends TestCase
{
    public function test_it_calculates_total_price_correctly(): void
    {
        $invoiceItems = [
            new InvoiceItem(Uuid::uuid4(), 'Product1', 2, new Money(10, 'usd')),
            new InvoiceItem(Uuid::uuid4(), 'Product1', 4, new Money(20, 'usd')),
        ];

        $invoice = new Invoice(
            Uuid::uuid4(),
            '555',
            new Company(
                Uuid::uuid4(),
                'Company',
                new Address('Wall Street', 'NY', '00110'),
                new PhoneNumber('22222222'),
                'email@example.com'
            ),
            new Company(
                Uuid::uuid4(),
                'BilledCompany',
                new Address('Wall Street', 'NY', '00110'),
                new PhoneNumber('11111111'),
                'email@example.com'
            ),
            new Date(new \DateTime()),
            new DueDate(new \DateTime()),
            new Status(StatusEnum::DRAFT->value),
            $invoiceItems
        );

        $this->assertEquals(100, $invoice->calculateTotalPrice());
    }
}
